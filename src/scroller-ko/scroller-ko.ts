/// <reference path="../../bower_components/polymer-decorators/polymer-decorators.d.ts" />
const { customElement, property } = Polymer.decorators;

@customElement('scroller-ko')
export class Scroller_ko extends Polymer.Element {
   hasScrolled: boolean;
   cache: { remaining: number, clientHeight: number, scrollHeight: number, scrollTop: number }
   timer: number;
   @property({ type: Object })
   elt: HTMLElement;
   constructor(elt?: HTMLElement) {
      super();
      this.hasScrolled = true;
      if(elt) {
         this.elt = elt;
         this.elt.addEventListener('scroll', this._throttle(this._scrollListener.bind(this)));
      } else
         this.elt = this as HTMLElement;
      this.cache = { clientHeight: 0, remaining: 0, scrollHeight: 0, scrollTop: 0 };
      this.timer = new Date().getTime();
   }

   ready() {
      super.ready();
      this.$.up.addEventListener('click', this.scrollUp.bind(this));
      this.$.down.addEventListener('click', this.scrollDown.bind(this));
   }

   _clientHeight() {
      if(this.hasScrolled) {
         return this.cache.clientHeight = this.elt.clientHeight;
      }
      else {
         return this.cache.clientHeight;
      }
   }
   _scrollHeight() {
      if(this.hasScrolled) {
         return this.cache.scrollHeight = this.elt.scrollHeight;
      }
      else {
         return this.cache.scrollHeight;
      }
   }
   _scrollTop() {
      if(this.hasScrolled) {
         return this.cache.scrollTop = this.elt.scrollTop;
      }
      else {
         return this.cache.scrollTop;
      }
   }

   remaining() {
      if(this.hasScrolled) {
         return this.cache.remaining = this._scrollHeight() - (this._clientHeight() + this._scrollTop())
      } else {
         return this.cache.remaining;
      }
   }

   scrollDown() {
      if(this.remaining() < this._clientHeight()) {
         const newScroll = this._scrollTop() + this.remaining();
         this._scrollTo(newScroll);
      } else if(this.remaining() == 0) {
      } else {
         const newScroll = this._scrollTop() + this._clientHeight();
         this._scrollTo(  newScroll );
      }
      // this.hasScrolled = false;
   }

   scrollUp() {
      if(this._scrollTop() > this._clientHeight()) {
         const newScroll = this._scrollTop() - this._clientHeight();
         this._scrollTo( newScroll );
      } else if(this.remaining() == 0) {
      } else {
         const newScroll = this._scrollTop() - this.remaining();
         this._scrollTo(  newScroll );
      }
      // this.hasScrolled = false;
   }

   _scrollListener(evt: UIEvent) {
      const elt = evt.currentTarget;
      
      if(elt) {
         return this.hasScrolled = true;
      }
   }

   _scrollTo(top: number) {
      this.elt.scrollTo({ behavior: 'smooth', top });
      this.hasScrolled = true;
      this.cache.scrollTop = top;
   }

   _throttle(func: ((_?) => any)) {
      return (evt:UIEvent)=> {
         const now = new Date().getTime();
         if(now - this.timer > 100) {
            this.timer = now;
            func(evt);
         }
      }   
   }
}