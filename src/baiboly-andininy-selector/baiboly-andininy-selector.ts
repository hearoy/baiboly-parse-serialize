/// <reference path="../../bower_components/polymer/types/polymer-element.d.ts" />
/// <reference path="../../bower_components/polymer-decorators/polymer-decorators.d.ts" />
import { BaibolyPuller } from '../baiboly-puller/baiboly-puller';
import { Andininy, Boky, BokyJson, BokyRefs, BaibolyJson, TokoJson, Toko, AndininyJson } from '../model/baiboly.model';
import { from as from$, fromEvent, Observable, map, merge, filter, take, switchMap, first, of, groupBy, concatMap, concatAll, tap, scan, staticConcat, concat, reduce, share } from '../rxjs-operators';


const { customElement, property, query } = Polymer.decorators;
/**
 * `baiboly-andininy-selector` Description
 *
 * @summary ShortDescription.
 * @customElement
 * @polymer
 * @extends {Polymer.Element}
 */
@customElement('baiboly-andininy-selector')
class BaibolyAndininySelector extends Polymer.Element {

   @property({ type: Array })
   dropdownAndininy: { id: String, idx: Number }[];
   
   @property({ type: Array })
   dropdownChapters: any;
   
	@property({type: Array})
   books: { id: String, label: String }[];
   
   @property({ type: String, notify: true })
   book: String;
   
   @property({ type: String, notify: true })
   chapter: String;   
   
   @property({ type: Object})
	selectedBook: HTMLElement;
   
   @property({ type: Object })
   baiboly: Observable<BaibolyJson>;
   
   @property({ type: Object })
   bokyRefs: Observable<BokyRefs[]>;
   
   @property({ type: String, notify: true })
   verseRef: String;

   /**
    * Instance of the element is created/upgraded. Use: initializing state,
    * set up event listeners, create shadow dom.
    * @constructor 
    */
	constructor() {
		super();
		this.books = []; 
		
   }
   
   __addOne(numb){ return Number(numb)+1}

   /**
    * Use for one-time configuration of your component after local DOM is initialized. 
    */
	connectedCallback() {
		super.connectedCallback();
		this.bokyRefs.pipe(
			switchMap(refs => from$(refs))
		).subscribe({
			next: ref => {
				this.push('books', { label: ref.label, id: ref.id });
			},
			error: () => { },
			complete: () => { }//(<any>this.$.booksRepeater).render();
		});
	}


	onBookSelected(evt: CustomEvent) {
      const bookElt = evt.detail.value as HTMLElement;//this.selectedBook;
      if(evt.detail.value == null) {
         return;
      }
		const bookId = bookElt.dataset.id;
		const bookIdx = Number(bookElt.dataset.idx as string);
		this.bokyRefs.pipe(
			map(books => books[bookIdx])
		)
			.subscribe(book => {
				this.dropdownChapters = book.chapters;
				// (<any>this.$.chaptersRepeater).render();
		})
	}
	onChapterSelected(evt: CustomEvent) {
      const chapterElt = evt.detail.value as HTMLElement;
      if(evt.detail.value == null) {
         return;
      }
		const chapterId = chapterElt.dataset.id;
		const chapterIdx = Number(chapterElt.dataset.idx as string);
		this.bokyRefs.pipe(
			map(books => books[Number(chapterElt.dataset.bookIdx)].chapters[chapterIdx])
		)
			.subscribe(chapter => {
				let anArray = new Array(chapter.verses).fill(0);
				this.dropdownAndininy = anArray.map((item, idx) => {
					return { id: [chapterId, idx+1].join('_'), idx: idx + 1 }
				});
				// (<any>this.$.versesRepeater).render();
		})
	}

}
