/// <reference path="../../bower_components/polymer/types/polymer-element.d.ts" />
/// <reference path="../../bower_components/polymer-decorators/polymer-decorators.d.ts" />
import { Andininy, BaibolySequenceItem } from '../model/baiboly.model';

const { customElement, property } = Polymer.decorators;

/**
 * `baiboly-slide` Description
 *
 * @summary ShortDescription.
 * @customElement
 * @polymer
 * @extends {Polymer.Element}
 */
@customElement('baiboly-slide')
class BaibolySlide extends Polymer.Element {
   
   @property({ type: Array })
   sequence: BaibolySequenceItem[];
   

   /**
    * Instance of the element is created/upgraded. Use: initializing state,
    * set up event listeners, create shadow dom.
    * @constructor
    */
   constructor() {
      super();
   }

   /**
    * Use for one-time configuration of your component after local DOM is initialized. 
    */
   ready() {
      super.ready();
   }

}