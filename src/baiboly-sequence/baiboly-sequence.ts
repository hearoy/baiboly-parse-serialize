/// <reference path="../../bower_components/polymer/types/polymer-element.d.ts" />
/// <reference path="../../bower_components/polymer/types/polymer.d.ts" />
/// <reference path="../../bower_components/polymer-decorators/polymer-decorators.d.ts" />
import { Andininy, BaibolySequenceItem } from '../model/baiboly.model';
import { Scroller_ko } from '../scroller-ko/scroller-ko';
const { customElement, property } = Polymer.decorators;

/**
 * `baiboly-slide` Description
 *
 * @summary ShortDescription.
 * @polymer
 * @extends {Polymer.Element}
 */
@customElement('baiboly-sequence')
class BaibolySequence extends Polymer.Element {

   mql: MediaQueryList;
   
   @property({ type: Number, observer: '_updateFontSizeChapter' })
   _fontSizeChapter: number;
   
   @property({ type: Number, observer: '_updateFontSize' })
   _fontSizeVerses: any;

   scroller: Scroller_ko;
   
   @property({ type: Object })
   _this = this;

   @property({ type: String })
   title = 'Vakiteny'
   
   @property({ type: Array })
   verses: BaibolySequenceItem[];

   /**
    * Instance of the element is created/upgraded. Use: initializing state,
    * set up event listeners, create shadow dom.
    * @constructor
    */
   constructor() {
      super();
      this.mql = window.matchMedia('screen and (max-width: 425px)');
      this.mql.addListener(this._onMediaQueryChange.bind(this));
   }

   connectedCallback() {
      super.connectedCallback();
      this.scroller = new Scroller_ko(this);
   }

   /**
    * Use for one-time configuration of your component after local DOM is initialized. 
    */
   ready() {
      super.ready();
      this._fontSizeVerses = 30;
      this._onMediaQueryChange(this.mql);
   }

   _onMediaQueryChange(evt) {
      if(evt.matches) {
         /* screen and (max-width: 425px) */
         this._fontSizeVerses = 16;
         this._fontSizeChapter = 22;
      } else {
         this._fontSizeVerses = 26;
         this._fontSizeChapter = 26;
      }
   }

   _updateFontSize(newV, oldV) {
      // @ts-ignore
      Polymer.updateStyles({
         // @ts-ignore
         '--baiboly-verse__fontSize': `${this._fontSizeVerses}px`
      });
   }
   _updateFontSizeChapter(newV, oldV) {
      // @ts-ignore
      Polymer.updateStyles({
         // @ts-ignore
         '--chapter-font-size': `${this._fontSizeChapter}px`
      });
   }

   book(lang: number) {
      return this.verses[0].bookNames[lang];
   }

   decreaseFontSize() {
      this._fontSizeVerses -= 2;
   }

   chapter(verse: Andininy) {
      return verse.chapterId.split('_').slice(1)[0];
   }

   increaseFontSize() {
      this._fontSizeVerses += 2;

   }

   scrollDown() {
      this.scroller.scrollDown();
   }
   scrollUp() {
      this.scroller.scrollUp();
   }

   showBook(index: string) {
      return Number(index) === 0;
   }

   showChapter(index: number, vIndex: number) {
      return index == 0 || vIndex == 1;
   }
   titleChapter(lang: string) {
      return Number(lang) === 0 ? 'Toko' : 'Chapitre';
   }

}