declare let cleanUp: {
    cleanUpChapitre: (p: HTMLElement) => void;
    cleanUpClusterMoyen: (sp: HTMLElement) => void;
    cleanUpLivre: (p: HTMLElement) => void;
    cleanUpUsuel: (sp: HTMLElement) => boolean | undefined;
};
/** Parse baiboly from http://www.madabibliq.org/Bible/BibleMalagasyHtm-Bible.htm
 * to an intermediate HTML.
 * Use `processBible()` and then `downloadBible()`
 */
declare class BaibolyParser {
    private inLivre;
    private numChapitre;
    private anchor;
    bible: HTMLElement;
    constructor();
    processBible(): void;
    downloadBible(): void;
    private processLivre(livreElt);
    private processChapitre(chElt);
    private processClusterMoyen(elt);
    /**
     * @description
     * @param {HTMLElement} usElt matches .Usuel
     * @returns {HTMLElement} usElt flattened by stripping the span child
     */
    private processAndininy(usElt);
}
declare let timeout: <T>(callback: any, timeout: any) => Promise<T>;
/**
 * @description Serialize the Baiboly in the intermediate HTML format given by the parser.
 * Stores a json with `serialize()` then it's downloadable with `downloadJson()`
 * @class BaibolySerializer
 */
declare class BaibolySerializer {
    bokyRefs: BokyRefs[];
    json: any;
    books: Boky[];
    anchor: HTMLAnchorElement;
    anchor2: HTMLAnchorElement;
    serialize(): void;
    downloadJson(): void;
    downloadBokyRefs(): void;
    toBokyRefs(): void;
}
declare class Andininy implements AndininyJson {
    private toko;
    bookId: String;
    bookName: String;
    /** id of the andininy, formatted as `(at|nt)#BOKY_#TOKO_#INDEX` */
    id: String;
    index: number;
    content: String;
    /**
     * Creates an instance of Andininy.
     * @param {(HTMLElement|any)} verse either the HTMLElement used to construct the json,
     * or the json object that just has {id, index, content}
     * @param {String} toko the chapter's id, like at33_2 for Ancient Testament, 33rd book, 2nd chapter
     * @memberof Andininy
     */
    constructor(verse: HTMLElement | any, toko: String, bookName?: String);
    readonly chapterId: String;
    static isAndininy(andininy: Andininy | any): andininy is Andininy | AndininyJson;
    toJSON(): {
        id: String;
        content: String;
        index: number;
        bookName: String;
    };
}
declare class Toko implements TokoJson {
    private boky;
    bookName: String | undefined;
    id: String;
    index: number;
    verses: Andininy[];
    constructor(toko: HTMLElement | any, boky: String, bookName?: String | undefined);
    readonly bookId: String;
    static isToko(toko: Toko | any): toko is Toko | TokoJson;
    toJSON(): {
        id: String;
        index: number;
        verses: Andininy[];
        bookName: String | undefined;
    };
    getVerses(from: number, to: number): Andininy[];
}
declare class Boky implements BokyJson {
    id: String;
    label: String;
    chapters: Toko[];
    constructor(boky: any | HTMLElement);
    static isBoky(boky: Boky | any): boky is Boky | BokyJson;
    toJSON(): {
        id: String;
        label: String;
        chapters: Toko[];
    };
    getChapter(which: number): Toko;
}
declare class BokyRefs {
    id: String;
    label: String;
    chapters: {
        id: String;
        verses: Number;
        bookName: String;
    }[];
    constructor(boky: Boky);
    static getChapterId(chapter: String, boky: String): string;
    static getVerseId(verse: String | Number, chapter: String | Number, boky?: String): string;
}
declare type BaibolyJson = BokyJson[];
interface BokyJson {
    id: String;
    label: String;
    chapters: TokoJson[];
}
interface TokoJson {
    id: String;
    index: number;
    verses: AndininyJson[];
    bookName?: String;
}
interface AndininyJson {
    /** id of the andininy, formatted as `(at|nt)#BOKY_#TOKO_#INDEX` */
    id: String;
    index: number;
    content: String;
    bookName?: String;
}
declare namespace BibleFr {
    class Serializer {
        anchor: any;
        pojo: BibleBook[];
        json: String;
        constructor();
        loadFile(input: HTMLInputElement): Promise<FileList>;
        readFileAsText(file: File): Promise<String>;
        __async(func: any): Promise<{}>;
        loadAndReadFile(): Promise<{
            json: String;
            pojo: any;
        }>;
        process(pojo: any): void;
        downloadJson(): void;
    }
    class BibleBook implements BibleBookJson {
        id: string;
        bnumber: number;
        bname: string;
        CHAPTER: Chapter[];
        constructor(obj: BibleBookJson);
        __buildId(bnumber: any): string;
        toJSON(): BokyJson;
    }
    class Chapter implements ChapterJson {
        boky: string;
        bookName: string;
        id: string;
        cnumber: number;
        VERS: Vers[];
        constructor(obj: ChapterJson, boky: string, bookName: string);
        toJSON(): TokoJson;
    }
    class Vers implements VersJson {
        toko: any;
        bookName: any;
        bookId: any;
        id: string;
        vnumber: number;
        text: string;
        constructor(obj: VersJson, toko: any, bookName: any, bookId: any);
        toJSON(): AndininyJson;
    }
    interface BibleBookJson {
        bnumber: number;
        bname: string;
        CHAPTER: ChapterJson[];
    }
    interface ChapterJson {
        cnumber: number;
        VERS: VersJson[];
    }
    interface VersJson {
        vnumber: number;
        text: string;
    }
}
