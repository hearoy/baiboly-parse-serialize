let cleanUp = (function() {
  const root = document.querySelector('.Section1');
  /**
   * set the id of `p a` to `p` and remove the anchor a
   * @param {HTMLElement} p matches('p.Chapitre')==true
   */
  function cleanUpChapitre(p: HTMLElement) {
    const a = p.querySelector('a');
    if(!a)
      return
    const id = a.getAttribute('name');
    p.setAttribute('id', id || '');
    p.removeChild(a);
  }

  /**
   * 
   * @param {HTMLElement} p matches('p.Livre')==true
   */
  function cleanUpLivre(p: HTMLElement) {
    const a = p.querySelector('a');
    if(!a)
      return
    const id = a.getAttribute('name');
    p.setAttribute('id', id || '');
    p.removeChild(a);
  }

  /**
   * @description Remove o:p and .Usuel > span if empty
   * @param {HTMLElement} sp matches('p.Usuel > span')==true
   * @returns {boolean} removed true if p.Usuel was empty and deleted
   */
  function cleanUpUsuel(sp: HTMLElement) {
    /** parent of .Usuel>span  */
    let par = sp.parentElement;
    /** grand parent */
    let gp;
    if(par)
      gp = par.parentElement;
    /** remove useless <o:p> */
    const ops = sp.getElementsByTagName('o:p');
    if(ops.length == 0)
      return;
    const op = ops[0];
    sp.removeChild(op);
    /** delete empty .Usuel>span */
    if(gp && sp.childNodes.length == 0) {
      gp.removeChild(par);
      return true;
    }
    return false;
  }

  /**
   * 
   * @param {HTMLElement} sp matches('p.Clustermoyen')==true
   */
  function cleanUpClusterMoyen(sp: HTMLElement) {
    let par = sp.parentElement;
    if(par)
      par.removeChild(sp);
  }
  return { cleanUpChapitre, cleanUpClusterMoyen, cleanUpLivre, cleanUpUsuel };
})();

/** Parse baiboly from http://www.madabibliq.org/Bible/BibleMalagasyHtm-Bible.htm
 * to an intermediate HTML.  
 * Use `processBible()` and then `downloadBible()`
 */
class BaibolyParser {
  private inLivre: boolean;
  private numChapitre: number;
  private anchor: HTMLElement;
  bible: HTMLElement;

  constructor() {
    this.inLivre = false;
    this.numChapitre = -1;
    this.bible = document.createElement('template');
  }

  processBible() {
    const start = Date.now();
    const bible = document.querySelector('.Section1');
    if(bible) {
      const newBible = document.createElement('section');
      newBible.classList.add('bible', 'baiboly');
      // bible.insertAdjacentElement('beforebegin', newBible);
      let next = bible.firstElementChild;
      while(next && !(next.matches('.Livre') || next.matches('Clustersuprieur'))) {
        next = next.nextElementSibling;
      }
      let newLivre, livre, newChapitre: Element | null;
      livre = null;
      newChapitre = null;
      while(next) {
        if(next.matches('.Livre')) {
          if(this.numChapitre == 0 && livre != null) {
            /* the Livre has no Chapitre
            so synthetically increment numChapitre so Usuel is processed
            in the artificially created Chapitre */
            next = livre;
            ++this.numChapitre;
            newLivre = null;
            newChapitre = document.createElement('p');
            newChapitre.classList.add('Chapitre', 'toko', 'chapitre');
            newChapitre.setAttribute('faha', '1');
          }
          else
            this.numChapitre = 0;

          livre = next;
          if(newLivre) {
            /** previous .Chapitre has been processed so append it */
            newLivre.appendChild(newChapitre);
            /** previous .Livre has been processed so append it */
            newBible.appendChild(newLivre);
            this.numChapitre = 0;
            performance.mark('livre--end');
            performance.measure('livre__measure', 'livre--start', 'livre--end');
          }
          performance.mark('livre--start');
          newLivre = this.processLivre(next as HTMLElement);
        }
        else if(next.matches('.Chapitre')) {
          if(this.numChapitre > 0) {
            newLivre.appendChild(newChapitre);
          }
          ++this.numChapitre;
          newChapitre = this.processChapitre(next as HTMLElement);
        }
        else if(next.matches('.Clustermoyen')) {
          this.processClusterMoyen(next as HTMLElement);
        }
        else if(next.matches('.Usuel')) {
          if(this.numChapitre < 1) { } // just skip this .Usuel if it's not part of a chapter
          else if(newChapitre) {
            // nt07_11 malformed, contained in .Usuel instead of .Chapitre
            // 
            if(next.textContent !== null && /Chapitre \d+/.test(next.textContent)) {
              next.classList.remove('Usuel');
              next.classList.add('Chapitre');
              if(/Chapitre \d+\S? *(\w \w)+/) { // at09_10 includes a verse
                const usuel = document.createElement('p');
                usuel.className = 'Usuel';
                const shouldHaveVerse = next.lastElementChild;
                if(shouldHaveVerse && shouldHaveVerse.matches('span')) { // move the verse to newly created .Usuel and make it next sibling
                  const span = next.lastElementChild;
                  if(span)
                    usuel.appendChild(span);
                  next.insertAdjacentElement('afterend', usuel);
                }
                const remaining = next.lastElementChild;
                if(remaining && !remaining.matches('span')) { // move up the span whose text content is Chapitre \d+
                  const span = remaining.firstElementChild; // it is nested in a b element
                  if(span && span.matches('span')) {
                    next.insertBefore(span, remaining);
                    remaining.remove();
                  }
                }
              }
                // next = next.previousElementSibling;
                continue;
            }  
            const andininy = this.processAndininy(next as HTMLElement);
            if(andininy.innerText.trim().length > 0)
              newChapitre.appendChild(andininy);
          }
        }
        if(next)
          next = next.nextElementSibling;
      }
      newBible.appendChild(newLivre);
      this.bible = newBible;
    }
    const finish = Date.now();
    const duration = (finish - start) / 1000;
    console.info('processing time : ' + duration);
  }

  downloadBible() {
    if(this.bible) {
      if(!this.anchor) {
        const wrapper = document.createElement('section');
        wrapper.appendChild(this.bible);
        const blob = new Blob([wrapper.innerHTML], { type: "text/html" });
        const url = URL.createObjectURL(blob);
        const a = document.createElement('a');
        a.download = 'baiboly-parsed.html';
        a.href = url;
        a.style.display = 'none';
        this.anchor = a;
        document.body.insertAdjacentElement('beforebegin', a);
      }
      this.anchor.click();
    }
  }

  private processLivre(livreElt: HTMLElement) {
    const livreCopy = livreElt.cloneNode(true) as HTMLElement;
    cleanUp.cleanUpLivre(livreCopy);
    const newLivreElt = document.createElement('section');
    if(livreCopy.classList.contains('Livre'))
      newLivreElt.classList.add('livre', 'boky');
    else
      newLivreElt.classList.add('livre--superieur', 'boky--superieur');
    newLivreElt.id = livreCopy.id;
    /** get the name of the Livre */
    newLivreElt.setAttribute('boky', livreCopy.innerText.replace(/(^\s)|(\s$)/g, ''));
    return newLivreElt;
  }

  private processChapitre(chElt: HTMLElement) {
    let offset = 0;
    /**
     * new div.chapitre element to replace p.Chapitre
     */
    //const newChElt = document.createElement('div');
    const newChElt = chElt.cloneNode(true) as HTMLElement;
    cleanUp.cleanUpChapitre(newChElt);
    /** 
     * next sibling to process is a p.Usuel or p.Posie containing one verse
     * @type {HTMLElement}
     */
    // let next = chElt.nextElementSibling;
    newChElt.classList.add('chapitre', 'toko');
    newChElt.id = chElt.id;

    const spanChild = newChElt.querySelector('span');
    if(spanChild) {
      /** remove the trailing `<o:p>` */
      const op = spanChild.getElementsByTagName('o:p');
      if(op.length > 0) {
        op[0].remove();
      }
      /** remove useless attributes */
      spanChild.removeAttribute('lang');
      spanChild.removeAttribute('style');
      spanChild.classList.add('chapitre__num', 'toko__faha');
      /** add number of the Chapitre to `span.chapitre__num`
       * and `.Chapitre` as a `[faha=tokoFaha]`*/
      const __tokoFahaS = spanChild.innerText.match(/\d+/);
      if(__tokoFahaS) {
        const tokoFaha = __tokoFahaS[0];
        newChElt.setAttribute('faha', tokoFaha);
        spanChild.setAttribute('faha', tokoFaha);
      }

    }
    // if(spanChild)
    //   newChElt.appendChild(spanChild.cloneNode(true));

    // const chParent = chElt.parentElement;
    // if(chParent)
    //   chParent.replaceChild(newChElt, chElt)
    return newChElt;
  }

  private processClusterMoyen(elt) {
    cleanUp.cleanUpClusterMoyen(elt.cloneNode(true) as HTMLElement);
  }

  /**
   * @description 
   * @param {HTMLElement} usElt matches .Usuel
   * @returns {HTMLElement} usElt flattened by stripping the span child
   */
  private processAndininy(usElt: HTMLElement) {
    let spanChild = usElt.querySelector('span');
    if(spanChild)
      spanChild = spanChild.cloneNode(true) as HTMLElement;
    const newElt = document.createElement('span');
    newElt.classList.add('andininy');
    if(spanChild) {
      cleanUp.cleanUpUsuel(spanChild);
      newElt.innerHTML = spanChild.innerText.trim();
      /** get number of andininy */
      const __numS = newElt.innerText.match(/^\d+/);
      if(__numS) {
        const andininyFaha = __numS[0];
        newElt.setAttribute('faha', andininyFaha);
      }
    }
    return newElt;
  }
}

let timeout = function <T>(callback, timeout) {
  return new Promise<T>(res => {
    setTimeout(() => res(callback()), timeout);
  })
}

/**
 * @description Serialize the Baiboly in the intermediate HTML format given by the parser.  
 * Stores a json with `serialize()` then it's downloadable with `downloadJson()`
 * @class BaibolySerializer
 */
class BaibolySerializer {
  bokyRefs: BokyRefs[];
  json: any;
  books: Boky[] = [];
  anchor: HTMLAnchorElement;
  anchor2: HTMLAnchorElement;

  serialize() {
    const baibolyH = document.querySelector('.baiboly');
    if(baibolyH) {
      const books = baibolyH.getElementsByClassName('boky');
      if(books) {
        const chain = [].reduce.call(books, (prs: Promise<Boky | null>, b: HTMLElement) => {
          return prs.then<Boky | null, never>(bo => timeout<Boky>(_b => new Boky(b as HTMLElement), 0)
          ).then(boky => {
            if(boky) {
              this.books.push(boky);
              return boky;
            } else
              return null
          });
        }, Promise.resolve(null));
        chain.then(() => { this.json = JSON.stringify(this.books); });
      }
    }
  }

  downloadJson() {
    if(this.json) {
      if(!this.anchor) {
        const blob = new Blob([this.json], { type: "application/json" });
        const url = URL.createObjectURL(blob);
        const a = document.createElement('a');
        a.download = 'baiboly.json';
        a.href = url;
        a.style.display = 'none';
        this.anchor = a;
        document.body.insertAdjacentElement('afterbegin', a);
      }
      this.anchor.click();
    }
  }
  downloadBokyRefs() {
    if(this.bokyRefs) {
      if(!this.anchor2) {
        const blob = new Blob([JSON.stringify(this.bokyRefs)], { type: "application/json" });
        const url = URL.createObjectURL(blob);
        const a = document.createElement('a');
        a.download = 'bokyRefs.json';
        a.href = url;
        a.style.display = 'none';
        this.anchor2 = a;
        document.body.insertAdjacentElement('afterbegin', a);
      }
      this.anchor2.click();
    }
  }

  toBokyRefs() {
    if(this.books) {
      this.bokyRefs = this.books.map(item => new BokyRefs(item));
    }
  }
}

class Andininy implements AndininyJson {
  bookId: String;
  bookName: String;
  /** id of the andininy, formatted as `(at|nt)#BOKY_#TOKO_#INDEX` */
  id: String;
  index: number;
  content: String;

  /**
   * Creates an instance of Andininy.
   * @param {(HTMLElement|any)} verse either the HTMLElement used to construct the json, 
   * or the json object that just has {id, index, content}
   * @param {String} toko the chapter's id, like at33_2 for Ancient Testament, 33rd book, 2nd chapter
   * @memberof Andininy
   */
  constructor(verse: HTMLElement | any, private toko: String, bookName?: String) {
    if(verse instanceof HTMLElement) {
      const index = verse.getAttribute('faha');
      this.id = toko + '_' + index;
      this.content = verse.innerText.replace(/^\d+ /, '');
      this.index = Number(index);
    } else {
      Object.assign(this, verse);
    }
    if(bookName) {
      this.bookName = bookName;
    }
    this.bookId = this.id.split('_').slice(0, 1)[0];
  }
  get chapterId(): String { return this.toko; }

  static isAndininy(andininy: Andininy | any): andininy is Andininy | AndininyJson {
    return (<Andininy>andininy).content !== undefined && typeof (<Andininy>andininy).content == typeof '';
  }

  toJSON() {
    const { id, content, index, bookName } = this;
    return { id, content, index, bookName };
  }


}

class Toko implements TokoJson {
  id: String;
  index: number;
  verses: Andininy[];

  constructor(toko: HTMLElement | any, private boky: String, public bookName?: String) {
    if(toko instanceof HTMLElement) {
      const index = toko.getAttribute('faha');
      this.id = this.boky + '_' + index;
      this.index = index ? Number(index) : -1;
      const verses = toko.querySelectorAll('.andininy');
      this.verses = [];
      for(let v of verses) {
        this.verses.push(new Andininy(v as HTMLElement, this.id, bookName));
      }
    } else {
      Object.assign(this, toko);
    }
  }
  get bookId(): String { return this.boky; }

  static isToko(toko: Toko | any): toko is Toko | TokoJson {
    return (<Toko>toko).verses !== undefined && (<Toko>toko).verses instanceof Array;
  }

  toJSON() {
    const { id, index, verses, bookName } = this;
    return { id, index, verses, bookName };
  }

  getVerses(from: number, to: number) {
    return this.verses.slice(from, to).map(item => new Andininy(item, this.id));
  }
}

class Boky implements BokyJson {
  id: String;
  label: String;
  chapters: Toko[];

  constructor(boky: any | HTMLElement) {
    if(boky instanceof HTMLElement) {
      this.id = boky.id;
      const label = boky.getAttribute('boky');
      this.label = label ? label : '';
      const chapters = boky.querySelectorAll('.toko');
      this.chapters = [];
      for(let ch of chapters.values()) {
        this.chapters.push(new Toko(ch as HTMLElement, this.id, this.label));
      }
    } else {
      const { id, label, chapters } = boky;
      this.id = id;
      this.label = label;
      // this.chapters = (<Array<any>>chapters).map(ch => new Toko(ch));
    }
  }

  static isBoky(boky: Boky | any): boky is Boky | BokyJson {
    return (<Boky>boky).label !== undefined && (<Boky>boky).chapters instanceof Array;
  }

  toJSON() {
    const { id, label, chapters } = this;
    return { id, label, chapters };
  }

  getChapter(which: number): Toko {
    // from(this.chapters).pipe(first(toko,idx)=>)
    return new Toko(this.chapters[which], this.id);
  }
}

class BokyRefs {
  id: String;
  label: String;
  chapters: { id: String, verses: Number, bookName: String }[];

  constructor(boky: Boky) {
    this.id = boky.id;
    this.label = boky.label;
    this.chapters = boky.chapters.map(ch => {
      const id = ch.id;
      const verses = ch.verses.length;
      return { id, verses, bookName: this.label };
    })
  }

  static getChapterId(chapter: String, boky: String) {
    return [boky, Number(chapter)].join('_');
  }
  static getVerseId(verse: String | Number, chapter: String | Number, boky?: String) {
    if(boky == undefined)
      return [chapter, Number(verse)].join('_');
    else
      return [boky, chapter, verse].join('_');
  }
}

type BaibolyJson = BokyJson[];
interface BokyJson {
  id: String;
  label: String;
  chapters: TokoJson[];
}
interface TokoJson {
  id: String;
  index: number;
  verses: AndininyJson[];
  bookName?: String;
}
interface AndininyJson {
  /** id of the andininy, formatted as `(at|nt)#BOKY_#TOKO_#INDEX` */
  id: String;
  index: number;
  content: String;
  bookName?: String;
}

namespace BibleFr {
  export class Serializer {
    anchor: any;
    pojo: BibleBook[];
    json: String;
    constructor() {
      this.loadAndReadFile().then(({ json, pojo }) => {
        this.process(pojo);
      });
    }

    loadFile(input: HTMLInputElement) {
      return new Promise<FileList>((res, rej) => {
        input.addEventListener('change', evt => {
          const files = input.files;
          if(files)
            res(files)
          else
            rej('file not loaded');
        });
        input.click();
      });
    }
    readFileAsText(file: File) {
      return new Promise<String>((res, rej) => {
        const fr = new FileReader();
        fr.onload = evt => {
          if(fr.readyState == 2) {
            res(fr.result as String);
          }
        };
        fr.readAsText(file);
      });
    }
    __async(func) {
      return new Promise(res => window.setTimeout(() => res(func()), 0));
    }
    loadAndReadFile() {
      const input = document.createElement('input') as HTMLInputElement;
      input.style.display = 'none';
      document.body.appendChild(input);
      input.type = "file";
      return this.loadFile(input)
        .then(files => this.readFileAsText(files[0]))
        .then(text => {
          return { json: text, pojo: JSON.parse(String(text)) };
        });
    }

    process(pojo: any) {
      const obj: BibleBookJson[] = pojo.XMLBIBLE.BIBLEBOOK;
      this.pojo = obj.map(book => new BibleBook(book));
    }

    downloadJson() {
      if(this.pojo) {
        if(!this.anchor) {
          this.json = JSON.stringify(this.pojo);
          const blob = new Blob([this.json], { type: "application/json" });
          const url = URL.createObjectURL(blob);
          const a = document.createElement('a');
          a.download = 'bibleFr.json';
          a.href = url;
          a.style.display = 'none';
          this.anchor = a;
          document.body.insertAdjacentElement('afterbegin', a);
        }
        this.anchor.click();
      }
    }

  }

  export class BibleBook implements BibleBookJson {
    id: string;
    bnumber: number;
    bname: string;
    CHAPTER: Chapter[];

    constructor(obj: BibleBookJson) {
      const { bnumber, bname } = obj;
      Object.assign(this, { bname, bnumber });
      this.id = this.__buildId(bnumber);
      if((<any>obj.CHAPTER).map != undefined) {
        this.CHAPTER = obj.CHAPTER.map(ch => new Chapter(ch, this.id, bname));
      } else {
        this.CHAPTER = [new Chapter(obj.CHAPTER as any, this.id, bname)]
      }
    }
    __buildId(bnumber) {
      let id: string;
      let faha: number;
      if(bnumber < 40) {
        id = "at";
        faha = bnumber % 40;
      } else {
        id = "nt";
        faha = bnumber % 40 + 1;
      }
      if(faha < 10) {
        id += "0" + faha;
      } else {
        id += faha;
      }
      return id;
    }
    toJSON(): BokyJson {
      const { bname: label, id } = this;
      const chapters = this.CHAPTER.map(ch => ch.toJSON());
      return { id, label, chapters };
    }
  }

  export class Chapter implements ChapterJson {
    id: string;
    cnumber: number;
    VERS: Vers[];
    constructor(obj: ChapterJson, public boky: string, public bookName: string) {
      const { cnumber } = obj;
      this.cnumber = cnumber;
      this.id = [boky, cnumber].join('_');
      this.VERS = obj.VERS.map(v => new Vers(v, this.id, bookName, boky));
    }

    toJSON(): TokoJson {
      const { id, cnumber: index, bookName } = this;
      const verses = this.VERS.map(v => v.toJSON());
      return { id, index, verses, bookName };
    }
  }

  export class Vers implements VersJson {
    id: string;
    vnumber: number;
    text: string;
    constructor(obj: VersJson, public toko, public bookName, public bookId) {
      Object.assign(this, obj);
      this.id = [toko, this.vnumber].join('_');
    }

    toJSON(): AndininyJson {
      const { id, text: content, vnumber: index, bookName } = this;
      return { id, content, index, bookName };
    }
  }

  export interface BibleBookJson {
    bnumber: number;
    bname: string;
    CHAPTER: ChapterJson[];

  }

  export interface ChapterJson {
    cnumber: number;
    VERS: VersJson[];
  }
  export interface VersJson {
    vnumber: number,
    text: string
  }
}