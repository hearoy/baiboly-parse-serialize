/// <reference path="../../bower_components/polymer/types/polymer-element.d.ts" />
/// <reference path="../../bower_components/polymer-decorators/polymer-decorators.d.ts" />
// import {customElement, property} from '../../bower_components/polymer-decorators/src/decorators'
import { BaibolyPuller } from '../baiboly-puller/baiboly-puller';
import { Andininy, Boky, BokyJson, BokyRefs, BaibolyJson, TokoJson, Toko, AndininyJson, BaibolySequenceItem } from '../model/baiboly.model';
import { from as from$, fromEvent, Observable, map, merge, filter, take, switchMap, first, of, groupBy, concatMap, concatAll, tap, scan, staticConcat, concat, reduce, share, toArray, subscribeOn, async, forkJoin, bufferCount, mergeMap } from '../rxjs-operators';

type BookRefStripped = { id: String, label: String };
const { customElement, property, query } = Polymer.decorators;
/**
 * `baiboly-dispatcher` Description
 *
 * @summary ShortDescription.
 * @customElement
 * @polymer
 * @extends {Polymer.Element}
 */
@customElement('baiboly-dispatcher')
class BaibolyDispatcher extends Polymer.Element {

	@property({ type: Object })
	baiboly: Observable<BaibolyJson[]>;
	
	bokyRefs: Observable<BokyRefs[]>;
	
	@property({ type: String })
	bookFrom: String;
	
	books: BookRefStripped[] = [];

	@property({ type: String })
	chapterFrom: String;

	@property({ type: Array, notify: true })
	sequences: BaibolySequenceItem[][];
	
	/**
	 * @description Andininy.id of the sequence's start
	 * @type {String}
	 * @memberof BaibolyDispatcher
	 */
	@property({ type: String })
	from: String;
	
	@property({ type: Object })
	selectedBook: HTMLElement;
	
	/**
	 * @description Andininy.id of the sequence's end
	 * @type {String}
	 * @memberof BaibolyDispatcher
	 */
	@property({ type: String })
	to: String;
	
	

   /**
    * Instance of the element is created/upgraded. Use: initializing state,
    * set up event listeners, create shadow dom.
    * @constructor 
    */
	constructor() {
		super();
		this.sequences = [];
		this.downloadRessources();
	}

   /**
    * Use for one-time configuration of your component after local DOM is initialized. 
    */
	ready() {
		super.ready();
		
		this.bokyRefs.pipe(
			switchMap(books => from$(books)),
			map(book => {
				return { id: book.id, label: book.label };
			}),
			subscribeOn(async)
		).subscribe(book => this.books.push(book));
	}

	downloadRessources() {
		const bp = new BaibolyPuller();
		this.baiboly = forkJoin(bp.getBaiboly(), bp.getBible());
		this.bokyRefs = bp.getBookRefs();
	}

   /**
    * @description Takes a starting and an ending reference well-formatted
    *  and returns an array containing the references of all the verses in-between
    * @param {String} from formatted as Book_Chapter_Verse
    * @param {String} to Book_Chapter_Verse
    * @returns {Observable<String[]>} the sequence of verses as a sequence of references
    * @memberof BaibolyDispatcher
    */
	input2refs(from: String, to: String): Observable<String[]> {
		const [bookF, chapterF, versesF] = from.split('_');
		const [bookT, chapterT, versesT] = to.split('_');
		const chapterFId = [bookF, chapterF].join('_');
		const chapterTId = [bookT, chapterT].join('_');
		let sequence$: Observable<String[]>;
		let sequence: String[] = [];
		if(chapterF.match(chapterT) !== null) { // if it stays in the same chapter
			const start = Number(versesF);
			const end = Number(versesT);
			for(let i = start; i <= end; i++) {
				sequence.push(
					[chapterFId, i].join('_')
				);
			}
			sequence$ = of(sequence);
		} else { // same book, different chapters
			sequence$ = this.bokyRefs.pipe(
				switchMap(array => from$(array))
				// ).pipe(
				//    filter(boky => {
				//       return boky.id == bookF || boky.id == bookT;
				//    }),
				//    take(2)
				// )
			).pipe(
				first(boky => boky.id == bookF),
				switchMap(boky => from$(boky.chapters)),
				filter(chapter => chapter.id == chapterFId || chapter.id == chapterTId),
				take(2)
			).pipe(
				reduce<{id:String, verses:Number}, String[]>((acc,chapterRefs) => {
					if(chapterRefs.id == chapterFId) {
						/** the starting chapter so start from the given verse until the last of the chapter */
						for(let i = Number(versesF); i <= chapterRefs.verses; i++) {
							acc.push(
								[chapterRefs.id, i].join('_')
							);
						}
					} else {
						/** ending chapter so start from the beginning of the chapter until the given verse */
						for(let i = 1; i <= Number(versesT); i++) {
							acc.push(
								[chapterRefs.id, i].join('_')
							);
						}
					}
					return acc;
				},[])
			);
		}
		return sequence$;
	}

	onProcessClick(evt: CustomEvent) {
		let bookRef: BookRefStripped;
		// TODO : add spinner
		this.process(this.from, this.to).subscribe();
	}

	process(from, to) {
		return this.input2refs(from, to).pipe(
			concatMap(refs => this.refs2verses(refs)),
			map(verseCouple => {
				const obj = verseCouple.map(it=>new Andininy(it, it.id.split('_').slice(0, 2).join('_')));
				// if(bookRef == undefined || bookRef.id == obj[0].bookId) {
				// 	bookRef = this.books.filter(boky => boky.id === obj[0].bookId)[0];
				// }
				// else {
				// 	obj.bookName = bookRef.label;
				// }
				return obj;
			}),
			map(andininy => {
				return new BaibolySequenceItem(...andininy);
			}),
			reduce<BaibolySequenceItem>((acc, item) => {
				return [...acc, item];
			}, [])
		).pipe(
			tap((verses: BaibolySequenceItem[]) => {
				this.push('sequences', verses);
			})
		).pipe(
			tap({
				next: v => {
					this.updateQueryString(0);
				}
			})
		);
	}

	processQueryString() {
		// ?bible=at01_1_1-at01_1_10,
		const searchParams = (new URL(location.href)).searchParams;
		const listOfSequences = searchParams.get('baiboly');
		const position = searchParams.get('pos');
		let title = searchParams.get('title');
		if(title != null && title != 'null')
		  title.replace('+', ' ');
		else
		  title = document.title;
		if(!listOfSequences)
		  return;
		const sequences = listOfSequences.split(',').map(couple => { 
			const [from, to] = couple.split('-');
			return { from, to };
		});
		return from$(sequences).pipe(
			concatMap(couple => {
				const { from, to } = couple;
				return this.process(from, to);
			})
		).pipe(
			tap({
			complete: () => {
					// @ts-ignore
					this.dispatchEvent(new CustomEvent<any>('baiboly-added', { composed: true, bubble: true }));
				}
			})
		).toPromise();
		// @ts-ignore
		
		
	 }

   /**
    * @description Takes references to verses and returns the verses in the languages stored in the state
    * @param {String[]} refs references of the verses formatted as Book_Chapter_Verse
    * @memberof BaibolyDispatcher
    */
	refs2verses(refs: String[]) {
		type Refs = { book: String, chapter: String, verse: String, verseId: String };
		const refsSplit = refs.map(ref => {
			const [book, chapter, verse] = ref.split('_');
			return { book, chapter, verse, verseId: ref };
		});
		// const andininy$ = from$(refsSplit).pipe(
		// 	concat(this.baiboly.pipe(switchMap(books=>from$(books))))
		// 	// stream of verse references to display and BokyJson-s to search into
		// ).pipe(
		// 	groupBy(item => { /** group Refs and whole books (Boky), grouped by book e.g. {book:"nt06", chapter, verse}, {id:"nt06", label, chapters:[...]} */
		// 		if(Boky.isBoky(item)){
		// 			return item.id;
		// 		}else
		// 			return item.book;
		// 	})
			
		// ).pipe(
		// 	concatMap(group => { // a group is composed of 1 book and many verses/references from different chapters
		// 		return group.pipe(
		// 			concatMap<BokyJson | Refs, any>((item: BokyJson | Refs) => { /** create a flat stream of Toko and Refs from the same book */
		// 				if(Boky.isBoky(item)) {
		// 					return from$(item.chapters);
		// 				} else
		// 					return of(item);
		// 			}),
		// 			tap((val: TokoJson | Refs) => {
		// 				console.log('<group-boky key= ' + group.key + '>');
		// 				console.dirxml(val);
		// 				console.log('</group-boky>');

		// 			}),
		// 			groupBy((item: TokoJson | Refs) => { /** group Refs and Toko of the same chapter */
		// 				let res;
		// 				if(Toko.isToko(item)) {
		// 					res= item.id;
		// 				} else {
		// 					res= [item.book, item.chapter].join('_');
		// 				}
		// 				return res;
		// 			}),
		// 		);
		// 	}),//.pipe(

		// 	concatMap((tokoGroup: Observable<TokoJson | Refs>) => {
		// 		return tokoGroup.pipe(
		// 			concatMap<TokoJson | Refs, any>(item => {
		// 				let res;
		// 				if(Toko.isToko(item)) {
		// 					res= from$(item.verses);
		// 				} else {
		// 					res= of(item);
		// 				}
		// 				return res;
		// 			}),
		// 			tap((val: AndininyJson | Refs) => {
		// 				console.log('<group-toko key= ' + (<any>tokoGroup).key + '>');
		// 				console.dirxml(val);
		// 				console.log('</group-toko>');
		// 			} ),
		// 			groupBy((item: AndininyJson | Refs) => {
		// 				let res;
		// 				if(Andininy.isAndininy(item)) {
		// 					res= item.id;
		// 				} else {
		// 					res= item.verseId;
		// 				}
		// 				console.log('res = ' + res);
		// 				return res;
		// 			}),
		// 			tap((val) => {
		// 				console.log('<group-andininy key= ' + val.key + '>');
		// 				console.dirxml(val);
		// 				// val.pipe(share(), concatAll()).subscribe(next=>console.dirxml(next));
		// 				console.log('</group-andininy>');
		// 			} ),
		// 			take(refsSplit.length) //take only as many groupedObservables as there are verses to display
		// 				)
		// 	})
		// ).pipe(
		// 	concatMap(obs => {
		// 		return obs.pipe(
		// 			filter(item => {
		// 				return Andininy.isAndininy(item);
		// 			})
		// 		)
		// 	})
		// );
		const andininy$ = this.baiboly.pipe(
			concatMap(bibles=>from$(bibles)),
			concatMap(books => from$(books).pipe(
				filter<BokyJson>(book => {
					return refsSplit.some(ref => ref.book == book.id);
				}),
				concatMap(books => from$(books.chapters).pipe(
					filter<TokoJson>(chapter => {
						return refsSplit.some(ref => [ref.book, ref.chapter].join('_') == chapter.id);
					})
				)),
				concatMap(chapters => from$(chapters.verses).pipe(
					filter<AndininyJson>(verse => {
						return refsSplit.some(ref => ref.verseId == verse.id);
					}),
					take(refs.length)
				))
				// here we have selected the andininy in one language
			)),
			groupBy(item => item.id),
			mergeMap(item => {
				return item.pipe(
					// tap(item => console.log(item)),
					bufferCount(2),
					// tap(item => console.log(item))
				)
			})
			
			
		);
		return andininy$;
			// .subscribe({
			// 	next: books => {
			// 		debugger;
			// 	}
			// })
	}

	updateQueryString(delay,event?){
		if(delay>0){
		  setTimeout(this.updateQueryString.bind(this,0,event),delay);
		  return;
		}
		const queryString = new URLSearchParams(location.search.slice(1));
		const hash = location.hash;
		const pattern = /[&?]?baiboly=(((at|nt)\d+_\d+_\d+)-((at|nt)\d+_\d+_\d+),?)+/;
		/* remove all previous sequences or pos in query string to recreate them from the found parameters */
		// let newQueryString = queryString.replace(pattern,'');
		/* get all hiras added and get the list of their hira-id */
		const sequences = this.sequences
			.map(seq => ([seq[0].id, seq[seq.length - 1].id].join('-')))
			.join(',');
		/* the query's part related to hiras */
		// const baibolyQuery = sequences.length ? `baiboly=${sequences}`:'';

		// /* if nothing else than hiras or pos was in the query */
		// if(newQueryString.length == 0){
		//   if(baibolyQuery.length == 0) /* no hiras were added */
		// 	 newQueryString =  '?' + position;
		//   else {
		// 	 newQueryString = `?${baibolyQuery}&${position}`;
		//   }

		// } else { /* if there was something else than hiras or pos then
		// restore the previous queryString */
		//   if(baibolyQuery.length == 0)
			//  newQueryString =  '?' + newQueryString + '&' + position;
		//   else { /* create it from scratch */
			//  newQueryString = `?${baibolyQuery}&${newQueryString}`;
		//   }
		// }
		queryString.set('baiboly', sequences);
		window.history.pushState(null, '', location.pathname + '?' + queryString.toString() + hash);
		// window.history.pushState(null, '', newQueryString.replace(/&*$/, '') + hash);
	 }

	__test() {
		this.input2refs('nt05_10_40', 'nt05_11_3').subscribe({
			// next: arr => staticConcat(this.refs2verses(arr)).subscribe(obs => {
			// 	debugger;
			// 	console.dirxml(obs);
			next: arr=> this.refs2verses(arr).subscribe(obs => {
					console.dirxml(obs);
			})
		});
	}

}
