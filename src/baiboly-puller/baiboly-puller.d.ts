import { Observable } from '../rxjs-operators';
import { BaibolyJson, BokyRefs } from '../model/baiboly.model';
export declare class BaibolyPuller {
    private baiboly;
    private bokyRefs;
    private requestBaiboly;
    private requestBokyRefs;
    static instance: BaibolyPuller | null;
    constructor();
    private _async(func);
    private _fetchBaiboly$();
    private _fetchBokyRef$();
    getBible(): Observable<BaibolyJson>;
    getBookRefs(): Observable<BokyRefs[]>;
}
