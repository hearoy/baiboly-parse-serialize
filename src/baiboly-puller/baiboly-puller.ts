import { fromPromise, Observable, async } from '../rxjs-operators';
import { BaibolyJson, BokyRefs } from '../model/baiboly.model';

export class BaibolyPuller {
   bibleFr: BokyJson[];
   private baiboly: BaibolyJson;
   private bokyRefs: BokyRefs[];
   private requestBaiboly;
   requestBibleFr: Request;
   private requestBokyRefs;
   static instance: BaibolyPuller | null = null;

   constructor() {
      if(BaibolyPuller.instance != null)
         return BaibolyPuller.instance;
      else {
         this.requestBaiboly = new Request('../data/baiboly.json', {
            method: 'get',
            cache: 'force-cache'
         });
         this.requestBibleFr = new Request('../data/bibleFr.json', {
            method: 'get',
            cache: 'force-cache'
         });
         this.requestBokyRefs = new Request('../data/bokyRefs.json', {
            method: 'get',
            cache: 'force-cache'
         });
         BaibolyPuller.instance = this;
      }
   }

   private _async(func: Function) {
      return new Promise(res => {
         setTimeout(() => {
            res(func());
         }, 0);
      })
   }
   private _fetchBaiboly$(): Observable<BaibolyJson> {
      let promiseBaiboly: Promise<BaibolyJson>;
      if(this.baiboly)
         promiseBaiboly = Promise.resolve(1).then<BaibolyJson>(res => this.baiboly);
      else
         promiseBaiboly = Promise.resolve(1).then(() => fetch(this.requestBaiboly)
            .then(res => res.json())
            .then(obj => {
               this.baiboly = obj;
               return this.baiboly;
            })

         );
      return fromPromise(promiseBaiboly, async);
   }
   private _fetchBible$(): Observable<BaibolyJson> {
      let promiseBible: Promise<BaibolyJson>;
      if(this.bibleFr)
         promiseBible = Promise.resolve(1).then<BaibolyJson>(res => this.bibleFr);
      else
         promiseBible = fetch(this.requestBibleFr)
            .then(res => res.json())
            .then(obj => {
               this.bibleFr = obj;
               return this.bibleFr;
            });
      return fromPromise(promiseBible, async);
   }

   private _fetchBokyRef$(): Observable<BokyRefs[]> {
      {//if(what.match('bokyRefs')){
         let promiseBokyRefs: Promise<BokyRefs[]>;
         if(this.bokyRefs)
            promiseBokyRefs = new Promise<BokyRefs[]>(res => res(this.bokyRefs));
         else
            promiseBokyRefs = Promise.resolve(1).then(() => fetch(this.requestBokyRefs)
               .then(res => res.json())
               .then(obj => {
                  this.bokyRefs = obj;
                  return this.bokyRefs;
               })
            );
         return fromPromise(promiseBokyRefs, async);
      }
   }

   getBaiboly(): Observable<BaibolyJson> {
      return this._fetchBaiboly$();
   }
   getBible(): Observable<BaibolyJson> {
      return this._fetchBible$();
   }

   getBookRefs() {
      return this._fetchBokyRef$();
   }
}