class Andininy implements AndininyJson{
  bookId: String;
  bookName: String;
  /** id of the andininy, formatted as `(at|nt)#BOKY_#TOKO_#INDEX` */
  id: String;
  index: number;
  content: String;

  /**
   * Creates an instance of Andininy.
   * @param {(HTMLElement|any)} verse either the HTMLElement used to construct the json, 
   * or the json object that just has {id, index, content}
   * @param {String} toko the chapter's id, like at33_2 for Ancient Testament, 33rd book, 2nd chapter
   * @memberof Andininy
   */
  constructor(verse: HTMLElement|any, private toko: String, bookName?: String) {
    if(verse instanceof HTMLElement) {
      const index = verse.getAttribute('faha');
      this.id = toko + '_' + index;
      this.content = verse.innerText.replace(/^\d+ /, '');
      this.index = Number(index);
    } else {
      Object.assign(this, verse);
    }
    if(bookName) {
      this.bookName = bookName;
    }
    this.bookId = this.id.split('_').slice(0, 1)[0];
  }
  get chapterId(): String { return this.toko; }

  static isAndininy(andininy: Andininy | any): andininy is Andininy|AndininyJson{
    return (<Andininy>andininy).content !== undefined && typeof (<Andininy>andininy).content == typeof '';
  }

  toJSON(): AndininyJson {
    const { id, content, index, bookName } = this;
    return { id, content, index, bookName };
  }


}

class Toko implements TokoJson{
  id: String;
  index: number;
  verses: Andininy[];

  constructor(toko: HTMLElement|any, private boky: String, public bookName?: String) {
    if(toko instanceof HTMLElement) {
      const index = toko.getAttribute('faha');
      this.id = this.boky + '_' + index;
      this.index = index ? Number(index) : -1;
      const verses = toko.querySelectorAll('.andininy');
      this.verses = [];
      for(let v of verses) {
        this.verses.push(new Andininy(v as HTMLElement, this.id, bookName));
      }
    } else {
      Object.assign(this, toko);
    }
  }
  get bookId(): String { return this.boky; }

  static isToko(toko: Toko | any): toko is Toko|TokoJson{
    return (<Toko>toko).verses !== undefined && (<Toko>toko).verses instanceof Array;
  }

  toJSON(): TokoJson {
    const { id, index, verses, bookName } = this;
    return { id, index, verses, bookName };
  }

  getVerses(from: number, to: number) {
    return this.verses.slice(from, to).map(item=>new Andininy(item, this.id));
  }
}

class Boky implements BokyJson{
  id: String;
  label: String;
  chapters: Toko[];

  constructor(boky: any | HTMLElement) {
    if(boky instanceof HTMLElement) {
      this.id = boky.id;
      const label = boky.getAttribute('boky');
      this.label = label ? label : '';
      const chapters = boky.querySelectorAll('.toko');
      this.chapters = [];
      for(let ch of chapters.values()) {
        this.chapters.push(new Toko(ch as HTMLElement, this.id,this.label));
      }
    } else {
      const { id, label, chapters } = boky;
      this.id = id;
      this.label = label;
      // this.chapters = (<Array<any>>chapters).map(ch => new Toko(ch));
    }
  }

  static isBoky(boky: Boky | any): boky is Boky|BokyJson{
    return (<Boky>boky).label !== undefined && (<Boky>boky).chapters instanceof Array;
  }

  toJSON() {
    const { id, label, chapters } = this;
    return { id, label, chapters };
  }

  getChapter(which: number): Toko {
    // from(this.chapters).pipe(first(toko,idx)=>)
    return new Toko(this.chapters[which], this.id);
  }
}

class BokyRefs {
  id: String;
  label: String;
  chapters: { id: String, verses: Number, bookName: String }[];

  constructor(boky: Boky) {
    this.id = boky.id;
    this.label = boky.label;
    this.chapters = boky.chapters.map(ch => {
      const id = ch.id;
      const verses = ch.verses.length;
      return { id, verses, bookName: this.label };
    })
  }

  static getChapterId(chapter: String, boky: String) {
    return [boky, Number(chapter)].join('_');
  }
  static getVerseId(verse: String|Number, chapter:String|Number, boky?: String) {
    if(boky == undefined)
      return [chapter, Number(verse)].join('_');
    else
      return [boky, chapter, verse].join('_');  
  }
}

type BaibolyJson = BokyJson[];
interface BokyJson {
  id: String;
  label: String;
  chapters: TokoJson[];
}
interface TokoJson {
  id: String;
  index: number;
  verses: AndininyJson[];
  bookName?: String;
}
interface AndininyJson {
  /** id of the andininy, formatted as `(at|nt)#BOKY_#TOKO_#INDEX` */
  id: String;
  index: number;
  content: String;
  bookName?: String;
}

class BaibolySequenceItem{
  bookNames: String[];
  bookId: String;
  chapterId: String;
  id: String;
  /** 1 verse in different languages
   */
  verse: Andininy[]
  constructor(...verse: Andininy[]) {
    const aVerse = verse[0];
    this.id = aVerse.id;
    this.verse = verse;
    this.bookId = aVerse.bookId;
    this.chapterId = aVerse.chapterId;
    this.bookNames = this.verse.map(v => v.bookName);
  }

}
export {  Andininy, AndininyJson, BaibolySequenceItem, BaibolyJson, Boky, BokyJson, Toko, TokoJson, BokyRefs };