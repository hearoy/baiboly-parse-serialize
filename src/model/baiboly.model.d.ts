declare class Andininy implements AndininyJson {
    private toko;
    /** id of the andininy, formatted as `(at|nt)#BOKY_#TOKO_#INDEX` */
    id: String;
    index: number;
    content: String;
    /**
     * Creates an instance of Andininy.
     * @param {(HTMLElement|any)} verse either the HTMLElement used to construct the json,
     * or the json object that just has {id, index, content}
     * @param {String} toko the chapter's id, like at33_2 for Ancient Testament, 33rd book, 2nd chapter
     * @memberof Andininy
     */
    constructor(verse: HTMLElement | any, toko: String);
    readonly chapterId: String;
    static isAndininy(andininy: Andininy | any): andininy is Andininy | AndininyJson;
    toJSON(): {
        id: String;
        content: String;
        index: number;
    };
}
declare class Toko implements TokoJson {
    private boky;
    id: String;
    index: number;
    verses: Andininy[];
    constructor(toko: HTMLElement | any, boky: String);
    readonly bookId: String;
    static isToko(toko: Toko | any): toko is Toko | TokoJson;
    toJSON(): {
        id: String;
        index: number;
        verses: Andininy[];
    };
    getVerses(from: number, to: number): Andininy[];
}
declare class Boky implements BokyJson {
    id: String;
    label: String;
    chapters: Toko[];
    constructor(boky: any | HTMLElement);
    static isBoky(boky: Boky | any): boky is Boky | BokyJson;
    toJSON(): {
        id: String;
        label: String;
        chapters: Toko[];
    };
    getChapter(which: number): Toko;
}
declare class BokyRefs {
    id: String;
    label: String;
    chapters: {
        id: String;
        verses: number;
    }[];
    constructor(boky: Boky);
    static getChapterId(chapter: String, boky: String): string;
    static getVerseId(verse: String | number, chapter: String | number, boky?: String): string;
}
interface BaibolySequenceItem {
    book: Boky;
    chapter: Toko;
    /** 1 verse in different languages
     */
    verse: Andininy[];
}
declare type BaibolyJson = BokyJson[];
interface BokyJson {
    id: String;
    label: String;
    chapters: TokoJson[];
}
interface TokoJson {
    id: String;
    index: number;
    verses: AndininyJson[];
}
interface AndininyJson {
    /** id of the andininy, formatted as `(at|nt)#BOKY_#TOKO_#INDEX` */
    id: String;
    index: number;
    content: String;
}
export { Andininy, AndininyJson, BaibolySequenceItem, BaibolyJson, Boky, BokyJson, Toko, TokoJson, BokyRefs };
