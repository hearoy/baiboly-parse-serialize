import * as webpack from 'webpack';
import * as HtmlWebpackPlugin from 'html-webpack-plugin';
import * as CopyWebpackPlugin from 'copy-webpack-plugin';
import * as CleanWebpackPlugin from 'clean-webpack-plugin';
import * as UglifyjsWebpackPlugin from 'uglifyjs-webpack-plugin';
import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer'
import * as ExtractTextPlugin from 'extract-text-webpack-plugin'
import * as CompressionPlugin from 'compression-webpack-plugin'
import * as WorkboxPlugin from 'workbox-webpack-plugin';
import * as path from 'path';


const PATHS = {
  entry: {
    vendor: [
      path.resolve(__dirname, 'src/rxjs-operators.ts'),
      path.resolve(__dirname, 'bower_components/polymer/polymer-element.html')
    ],
    app: path.resolve(__dirname, 'src/baiboly_reveal-app/baiboly_reveal-app.html')

  },
  outputDir: path.resolve(__dirname, 'dist'),
  index: path.resolve(__dirname, 'src/index.ejs')
};
const configBaiboly: webpack.Configuration = {
  // Tell Webpack which file kicks off our app.
  entry: PATHS.entry,
  // Tell Weback to output our bundle to ./dist/bundle.js
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist')
  },
  // Tell Webpack which directories to look in to resolve import statements.
  // Normally Webpack will look in node_modules by default but since we’re overriding
  // the property we’ll need to tell it to look there in addition to the
  // bower_components folder.
  resolve: {
    extensions: ['.ts', '.js', '.html'],
    modules: [
      path.resolve(__dirname, 'node_modules'),
      path.resolve(__dirname, 'bower_components')
    ]
  },
  // These rules tell Webpack how to process different module types.
  // Remember, *everything* is a module in Webpack. That includes
  // CSS, and (thanks to our loader) HTML.
  module: {
    rules: [
      {
        // If you see a file that ends in .html, send it to these loaders.
        test: /\.html$/,
        // This is an example of chained loaders in Webpack.
        // Chained loaders run last to first. So it will run
        // polymer-webpack-loader, and hand the output to
        // babel-loader. This let's us transpile JS in our `<script>` elements.
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [['env', {
                exclude: ["transform-es2015-modules-commonjs"]
              }]]
            }
          },
          // {loader: 'ts-loader'},
          { loader: 'polymer-webpack-loader' }
        ]
      },
      {
        // If you see a file that ends in .js, just send it to the babel-loader.
        test: /\.js$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['env']
            }
          }
        ],
        // Optionally exclude node_modules from transpilation except for polymer-webpack-loader:
        // exlude web-animations-js/* because it doesn't support "use strict" applied by babel
        exclude: [/node_modules\/(?!polymer-webpack-loader\/).*/,
          /bower_components\/web-animations-js\/.*/, /bower_components\/webcomponentsjs/]
      },
      {
        test: /web-animations.*\.min\.js$/,
        use: 'imports-loader?this=>window',
      },
      {
        // If you see a file that ends in .js, just send it to the babel-loader.
        test: /\.ts$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['env']
            }
          },
          {
            loader: 'ts-loader'
          }
        ]
        // Optionally exclude node_modules from transpilation except for polymer-webpack-loader:
        // exclude: /node_modules\/(?!polymer-webpack-loader\/).*/
      }
    ]
  },
  // Enable the Webpack dev server which will build, serve, and reload our
  // project on changes.
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 9000
  },
  devtool: 'source-map',
  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: 2
      // chunks: ['polymer', 'app']
    }),
    // new webpack.optimize.CommonsChunkPlugin({
    //   name: 'rxjs',
    //   chunks: ['rxjs', 'app']
    // }),
    
    // new webpack.optimize.CommonsChunkPlugin({
    //   name: 'webpack',
    //   minChunks: Infinity
    // }),
    new UglifyjsWebpackPlugin({
      cache: false,
      parallel: false,
      uglifyOptions: {
        ecma: 5
      }
    }),
    new CleanWebpackPlugin([PATHS.outputDir]),

    // This plugin will copy files over for us without transforming them.
    // That's important because the custom-elements-es5-adapter.js MUST
    // remain in ES2015.
    new CopyWebpackPlugin([
      {
        from: 'bower_components/webcomponentsjs/*.js',
        to: 'bower_components/webcomponentsjs/[name].[ext]',
        force: true
      },
      {
        from: path.resolve(__dirname, 'src/data/*.json'),
        to: path.resolve(PATHS.outputDir, 'data/[name].[ext]')
      }],
      { debug: 'info' }),
    // This plugin will generate an index.html file for us that can be used
    // by the Webpack dev server. We can give it a template file (written in EJS)
    // and it will handle injecting our bundle for us.
    new HtmlWebpackPlugin({
      // excludeChunks: ['webpack', 'common'],
      // chunksSortMode: function (chunk1, chunk2) {
      //   var orders = ['rxjs', 'polymer', 'app'];
      //   var order1 = orders.indexOf(chunk1.names[0]);
      //   var order2 = orders.indexOf(chunk2.names[0]);
      //   if (order1 > order2) {
      //     return 1;
      //   } else if (order1 < order2) {
      //     return -1;
      //   } else {
      //     return 0;
      //   }
      // },
      template: PATHS.index,
      inject: 'body',
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeRedundantAttributes: true,
        useShortDoctype: true,
        removeEmptyAttributes: true,
        removeStyleLinkTypeAttributes: true,
        keepClosingSlash: true,
        minifyJS: true,
        minifyCSS: true,
        minifyURLs: true,
      },
    }),
    new BundleAnalyzerPlugin({
      analyzerMode: 'static'
    })
  ]
};

const FIHIRANA_PATHS = {
  entry: {
    fihiranaShell: path.resolve(__dirname, 'fihirana-reveal','./app-shell.html')
  },
  outputDir: path.resolve(__dirname, 'fihirana-reveal', 'dist'),
  index: 'index.ejs',
  base: path.resolve(__dirname, 'fihirana-reveal')
}
/**                               **/
/** FIHIRANA REVEAL configuration  */
/**                               **/
const configFihirana: webpack.Configuration = {
  context: path.resolve(__dirname, 'fihirana-reveal/'),
  // Tell Webpack which file kicks off our app.
  entry: FIHIRANA_PATHS.entry,
  // Tell Weback to output our bundle to ./dist/bundle.js
  output: {
    filename: '[name].js',
    path: FIHIRANA_PATHS.outputDir
  },
  devServer: {
    contentBase: 'dist',
    compress: true,
    port: 9000
  },
  // Tell Webpack which directories to look in to resolve import statements.
  // Normally Webpack will look in node_modules by default but since we’re overriding
  // the property we’ll need to tell it to look there in addition to the
  // bower_components folder.
  resolve: {
    extensions: [ '.js', '.html', '.css'],
    modules: [
      path.resolve(__dirname,'node_modules'),
      path.resolve(__dirname,'bower_components')
    ]
  },
  resolveLoader: {
    modules: [path.resolve(__dirname), 'node_modules'],
  },
  // These rules tell Webpack how to process different module types.
  // Remember, *everything* is a module in Webpack. That includes
  // CSS, and (thanks to our loader) HTML.
  module: {
    rules: [
      {
        // If you see a file that ends in .html, send it to these loaders.
        test: /\.html$/,
        // This is an example of chained loaders in Webpack.
        // Chained loaders run last to first. So it will run
        // polymer-webpack-loader, and hand the output to
        // babel-loader. This let's us transpile JS in our `<script>` elements.
        use: [
        
          // { loader: 'style-loader/url', options: { insertAt: 'top' } },
          // { loader: 'css-loader', options: { minimize: true } },
          // { loader: 'file-loader' },
          
          {
            loader: 'babel-loader',
            options: {
              presets: [['env', {
                exclude: ["transform-es2015-modules-commonjs"]
              }]]
            }
          },
          
          // {loader: 'ts-loader'},
          {
            loader: 'polymer-webpack-loader',
            options: {
              processStyleLinks: true,
              htmlLoader: {
                attrs: ['link:href']
              }
            }
          }
        ]
      },
      {
        // If you see a file that ends in .js, just send it to the babel-loader.
        test: /\.js$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['env']
            }
          }
        ],
        // Optionally exclude node_modules from transpilation except for polymer-webpack-loader:
        // exlude web-animations-js/* because it doesn't support "use strict" applied by babel
        exclude: [/node_modules\/(?!polymer-webpack-loader\/).*/,
          /bower_components\/web-animations-js\/.*/, /bower_components\/webcomponentsjs/]
      },
      {
        test: /web-animations.*\.min\.js$/,
        use: 'imports-loader?this=>window',
      },
      {
        test: /\.css$/,
        use:
          [
            {
              loader: 'file-loader',
              options: {
                useRelativePath: true,
                name: '[name].[ext]'
              }
            },
          // {loader: 'to-string-loader'},
          //   {
          //       loader: 'css-loader',
          //       options: {
          //         minimize: false,
          //         url: true,
          //         import: true,
          //         importLoaders: 2
          //       }
          //     }
          // { loader: 'extract-loader' },
          // {
          //   loader: 'resolve-url-loader'
          // }
        ]
          // { loader: 'style-loader/url' },
          // { loader: 'extract-loader' },
          // { loader: 'css-loader', options: {minimize: true, url:false, import:false} }
        //   ExtractTextPlugin.extract({
        //     fallback: 'style-loader',
        //     use: [
        //       {
        //         loader: 'css-loader',
        //         options: {

        //         }
        //       },
        //       {
        //         loader: 'resolve-url-loader'
        //       }
        //     ]
            
        // })
        
      },
      {
        test: /\.(ttf|eot|woff2?|svg)(\?.*)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              useRelativePath: true,
              name: '[path][name].[ext]'
            }
          }
        ]
      }
      
    ]
  },
  devtool: 'source-map',
  plugins: [
    // new ExtractTextPlugin({
    //   filename: 'css/[name].css',
    //   allChunks: true
    // }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'common',
      minChunks: 2
      // chunks: ['polymer', 'app']
    }),
    // new webpack.optimize.CommonsChunkPlugin({
    //   name: 'rxjs',
    //   chunks: ['rxjs', 'app']
    // }),
    
    // new webpack.optimize.CommonsChunkPlugin({
    //   name: 'webpack',
    //   minChunks: Infinity
    // }),
    new UglifyjsWebpackPlugin({
      cache: false,
      parallel: false,
      uglifyOptions: {
        ecma: 5
      }
    }),
    new CleanWebpackPlugin([FIHIRANA_PATHS.outputDir]),

    // This plugin will copy files over for us without transforming them.
    // That's important because the custom-elements-es5-adapter.js MUST
    // remain in ES2015.
    new CopyWebpackPlugin([
      {
        from: 'bower_components/webcomponentsjs/*.js',
        to: 'bower_components/webcomponentsjs/[name].[ext]'
      },
      {
        from: 'css/print/*.css',
        to: 'css/print/[name].[ext]'
      },
      {
        from: 'res/*',
        to: 'res/[name].[ext]'
      },
      // {
      //   from: 'fihirana-revealjs.zip',
      //   to: '[name].[ext]'
      // },
      {
        from: 'js/reveal.min.js',
        to: 'js/[name].[ext]'
      },
      {
        from: 'fihirana.json',
        to: '[name].[ext]'
      },
      {
        from: 'fihirana.html',
        to: '[name].[ext]'
      },
      {
        from: 'lib/font/**/*',
        to: './',
      },
      
    ],
      { debug: 'info' }),
    // This plugin will generate an index.html file for us that can be used
    // by the Webpack dev server. We can give it a template file (written in EJS)
    // and it will handle injecting our bundle for us.
    new HtmlWebpackPlugin({
      // excludeChunks: ['webpack', 'common'],
      // chunksSortMode: function (chunk1, chunk2) {
      //   var orders = ['rxjs', 'polymer', 'app'];
      //   var order1 = orders.indexOf(chunk1.names[0]);
      //   var order2 = orders.indexOf(chunk2.names[0]);
      //   if (order1 > order2) {
      //     return 1;
      //   } else if (order1 < order2) {
      //     return -1;
      //   } else {
      //     return 0;
      //   }
      // },
      template: path.join(FIHIRANA_PATHS.base,FIHIRANA_PATHS.index),
      inject: 'body',
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeRedundantAttributes: true,
        useShortDoctype: true,
        removeEmptyAttributes: true,
        removeStyleLinkTypeAttributes: true,
        keepClosingSlash: true,
        minifyJS: true,
        minifyCSS: true,
        minifyURLs: true,
      },
    }),
    new BundleAnalyzerPlugin({
      analyzerMode: 'static'
    })
  ]
}

const revealCopy = [
  {
    from: 'bower_components/webcomponentsjs/*.js',
    to: 'bower_components/webcomponentsjs/[name].[ext]'
  },
  {
    from: 'css/print/*.css',
    to: 'css/print/[name].[ext]'
  },
  {
    from: 'res/*',
    to: 'res/[name].[ext]'
  },
  // {
  //   from: 'fihirana-revealjs.zip',
  //   to: '[name].[ext]'
  // },
  {
    from: 'js/reveal.min.js',
    to: 'js/[name].[ext]'
  },
  {
    from: 'fihirana.json',
    to: '[name].[ext]'
  },
  {
    from: 'fihirana.html',
    to: '[name].[ext]'
  },
  {
    from: 'lib/font/',
    to: 'lib/font/[path][name].[ext]',
    toType: 'template'
  },
  
];

const revealMixCopy = revealCopy.map(ft => {
  if(! /bower_components/.test(ft.from)) {
    return { from: path.resolve(FIHIRANA_PATHS.base, ft.from), to: ft.to }
  } else {
    return ft;
  }
});

let mix = function(o1: any, o2: any) {
  let o = {};
  return Object.assign(o, o1, o2);
}
let MIX = {
  outputDir: path.resolve(__dirname, 'mixDist'),
  index: path.resolve(__dirname, 'index.mix.ejs')
}
/**                               **/
/** FIHIRANA sy Baiboly configuration  */
/**                               **/

const configMix: webpack.Configuration = {
  // Tell Webpack which file kicks off our app.
  entry: mix(FIHIRANA_PATHS.entry,PATHS.entry),
  // Tell Weback to output our bundle to ./dist/bundle.js
  output: {
    filename: '[name].js',
    path: MIX.outputDir,
    // publicPath: 'https://fanipazana.fpmaorleans.fr'
  },
  devServer: {
    contentBase: MIX.outputDir,
    compress: true,
    https: true,
    open: false,
    overlay: true,
    port: 9000
  },
  // Tell Webpack which directories to look in to resolve import statements.
  // Normally Webpack will look in node_modules by default but since we’re overriding
  // the property we’ll need to tell it to look there in addition to the
  // bower_components folder.
  resolve: {
    extensions: [ '.ts', '.js', '.html', '.css'],
    modules: [
      'node_modules',
      'bower_components'
    ]
  },
  resolveLoader: {
    modules: [path.resolve(__dirname), 'node_modules'],
  },
  // These rules tell Webpack how to process different module types.
  // Remember, *everything* is a module in Webpack. That includes
  // CSS, and (thanks to our loader) HTML.
  module: {
    rules: [
      {
        // If you see a file that ends in .html, send it to these loaders.
        test: /\.html$/,
        // This is an example of chained loaders in Webpack.
        // Chained loaders run last to first. So it will run
        // polymer-webpack-loader, and hand the output to
        // babel-loader. This let's us transpile JS in our `<script>` elements.
        use: [
        
          // { loader: 'style-loader/url', options: { insertAt: 'top' } },
          // { loader: 'css-loader', options: { minimize: true } },
          // { loader: 'file-loader' },
          
          {
            loader: 'babel-loader',
            options: {
              presets: [['env', {
                exclude: ["transform-es2015-modules-commonjs"]
              }]]
            }
          },
          
          // {loader: 'ts-loader'},
          {
            loader: 'polymer-webpack-loader',
            options: {
              processStyleLinks: true,
              htmlLoader: {
                attrs: ['link:href']
              }
            }
          }
        ]
      },
      {
        // If you see a file that ends in .js, just send it to the babel-loader.
        test: /\.js$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['env']
            }
          }
        ],
        // Optionally exclude node_modules from transpilation except for polymer-webpack-loader:
        // exlude web-animations-js/* because it doesn't support "use strict" applied by babel
        exclude: [/node_modules\/(?!polymer-webpack-loader\/).*/,
          /bower_components\/web-animations-js\/.*/, /bower_components\/webcomponentsjs/]
      },
      {
        // If you see a file that ends in .js, just send it to the babel-loader.
        test: /\.ts$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['env']
            }
          },
          {
            loader: 'ts-loader'
          }
        ]
        // Optionally exclude node_modules from transpilation except for polymer-webpack-loader:
        // exclude: /node_modules\/(?!polymer-webpack-loader\/).*/
      },
      {
        test: /web-animations.*\.min\.js$/,
        use: 'imports-loader?this=>window',
      },
      {
        test: /polymer-decorators\.js$/,
        use: 'imports-loader?this=>window',
      },
      {
        test: /\.css$/,
        use:
          [
            {
              loader: 'file-loader',
              options: {
                useRelativePath: true,
                name: '[name].[ext]'
              }
            },
          // {loader: 'to-string-loader'},
          //   {
          //       loader: 'css-loader',
          //       options: {
          //         minimize: false,
          //         url: true,
          //         import: true,
          //         importLoaders: 2
          //       }
          //     }
          // { loader: 'extract-loader' },
          // {
          //   loader: 'resolve-url-loader'
          // }
        ]
          // { loader: 'style-loader/url' },
          // { loader: 'extract-loader' },
          // { loader: 'css-loader', options: {minimize: true, url:false, import:false} }
        //   ExtractTextPlugin.extract({
        //     fallback: 'style-loader',
        //     use: [
        //       {
        //         loader: 'css-loader',
        //         options: {

        //         }
        //       },
        //       {
        //         loader: 'resolve-url-loader'
        //       }
        //     ]
            
        // })
        
      },
      {
        test: /\.(ttf|eot|woff2?|svg)(\?.*)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              useRelativePath: true,
              name: '[path][name].[ext]'
            }
          }
        ]
      }
      
    ]
  },
  devtool: 'source-map',
  plugins: [
    // new ExtractTextPlugin({
    //   filename: 'css/[name].css',
    //   allChunks: true
    // }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: 2
      // chunks: ['polymer', 'app']
    }),
    // new webpack.optimize.CommonsChunkPlugin({
    //   name: 'rxjs',
    //   chunks: ['rxjs', 'app']
    // }),
    
    // new webpack.optimize.CommonsChunkPlugin({
    //   name: 'webpack',
    //   minChunks: Infinity
    // }),
    
    new CleanWebpackPlugin([MIX.outputDir]),

    // This plugin will copy files over for us without transforming them.
    // That's important because the custom-elements-es5-adapter.js MUST
    // remain in ES2015.
    new CopyWebpackPlugin(
      revealMixCopy.concat([
      {
        from: path.resolve(__dirname, 'src/data/*.json'),
        to: 'data/[name].[ext]'
        },
        {
          from: path.resolve(FIHIRANA_PATHS.base, 'subsites/'),
          to: './[path][name].[ext]',
          toType: 'template'
        },
        {
          from: path.resolve(FIHIRANA_PATHS.base, 'manifest.json'),
          to: './'
        },
        {
          from: path.resolve(FIHIRANA_PATHS.base, 'res/icons/*.png'),
          to: 'res/icons',
          //@ts-ignore
          flatten: true
        }
    ]),
      { debug: 'info' }),
    // This plugin will generate an index.html file for us that can be used
    // by the Webpack dev server. We can give it a template file (written in EJS)
    // and it will handle injecting our bundle for us.
    new HtmlWebpackPlugin({
      // excludeChunks: ['webpack', 'common'],
      // chunksSortMode: function (chunk1, chunk2) {
      //   var orders = ['rxjs', 'polymer', 'app'];
      //   var order1 = orders.indexOf(chunk1.names[0]);
      //   var order2 = orders.indexOf(chunk2.names[0]);
      //   if (order1 > order2) {
      //     return 1;
      //   } else if (order1 < order2) {
      //     return -1;
      //   } else {
      //     return 0;
      //   }
      // },
      chunkOrder: ['vendor','fihiranaShell','app'],
      template: MIX.index,
      inject: false,
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeRedundantAttributes: true,
        useShortDoctype: true,
        removeEmptyAttributes: true,
        removeStyleLinkTypeAttributes: true,
        keepClosingSlash: true,
        minifyJS: true,
        minifyCSS: true,
        minifyURLs: true,
      },
    }),
    new BundleAnalyzerPlugin({
      analyzerMode: 'static'
    }),
    new CompressionPlugin({
      test: /\.(css|js|html)/,
      asset: '[path].gz',
      deleteOriginalAssets: false
    })
  ]
}
export default (env: any) => {
  /** config workbox for production  
   * because server mode throws a glob error
   */
  const workboxPlugin = new WorkboxPlugin({
    globDirectory: 'mixDist',
    globPatterns: ['**/!(gulpfile|webcomponents*).{html,js,css}',
      'bower_components/webcomponentsjs/webcomponents-loader.js',
      'data/bibleFr.json',  
      'data/bokyRefs.json',
      'data/baiboly.json',
      'fihirana.json',
      'fihirana.html',
      '**/PlayfairDisplay-Bold.woff2*',
     '**/OpenSans-Semibold.woff2*',
     '**/OpenSans-Bold.woff2*',
     '**/OpenSans-SemiboldItalic.woff2*'
    ],
    swDest: path.join('mixDist', 'sw.js'),
    maximumFileSizeToCacheInBytes: 6350000,
    clientsClaim: true,
    skipWaiting: true,
    directoryIndex: 'index.html',
    ignoreUrlParametersMatching: [/./],
    fetch: true,
    runtimeCaching: [
      {
        urlPattern: new RegExp('https://fanipazana.fpmaorleans.fr/.*\\.(css|js|json|html|jpg|png)'),
        handler: 'staleWhileRevalidate'
      },
      {
        urlPattern: new RegExp('https://localhost:\\d+/.*\\.(css|js|json|html|jpg|png)'),
        handler: 'staleWhileRevalidate'
      },
      {
        urlPattern: new RegExp('https://fonts.googleapis.com/css'),
        handler: 'staleWhileRevalidate'
      }
    ]
  });
  /** config uglifyJs with source maps */
  const uglifyjsDev = new UglifyjsWebpackPlugin({
    cache: false,
    parallel: false,
    uglifyOptions: {
      ecma: 5
    },
    sourceMap: true
  });

  if(env && env.gitlab) {
    configMix.output.publicPath = 'https://fanipazana.fpmaorleans.fr/';
    configMix.plugins.push(workboxPlugin,
      new UglifyjsWebpackPlugin({
        cache: false,
        parallel: false,
        uglifyOptions: {
          ecma: 5
        }
      })
    );
  } else if(env && env.serve) {
    /** no workbox when watching and serving */
    configMix.plugins.push(uglifyjsDev);
    configMix.devtool = "eval-source-map"
  } else {
    configMix.plugins.push(
      uglifyjsDev, workboxPlugin
    )
  }
  
  return configMix
};